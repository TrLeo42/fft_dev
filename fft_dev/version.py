""" fft_dev/version.py """
__version__ = "0.4.2a0"

# 0.4.2a0 (20/11/2019): Correct bugged frequency response acquisition.
#                       Add selection of resolution. Correct sync mechanism.
#                       And bugs correction.
# 0.4.1a0 (27/06/2019): Update UI with respect to the wish of users.
# 0.4.0a0 (21/03/2019): Move to PyQt5. Add preliminary support for HP35670A
#                       signal analyzer: 'fft3562a' package now becomes
#                       a package for signal analyzer device 'fft_dev'.
# 0.3.0a2 (08/12/2016): Change dependence from ioben to iopy.
# 0.3.0a1 (02/11/2016): Correct problem with usage of bytes instead of string
#                       in Python 3.
# 0.3.0a0 (20/01/2016): Move to Python 3.
